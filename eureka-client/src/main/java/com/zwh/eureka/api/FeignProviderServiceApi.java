package com.zwh.eureka.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * 统一管理服务提供者的api接口，
 * 指定服务提供者的服务名
 * @author zwh
 * @date 2021/7/30 16:58
 **/
@FeignClient(value = "eureka-client2")
public interface FeignProviderServiceApi {

    /**
     * provider服务的接口方法，在这里管理
     * @param string
     * @return
     */
    @GetMapping(value = "/echo/{string}")
    String providerEcho(@PathVariable String string);

    @PostMapping(value = "/echo/{string}")
    String providerEcho(@PathVariable String string, @RequestBody Map<String, Object> dto);
}
