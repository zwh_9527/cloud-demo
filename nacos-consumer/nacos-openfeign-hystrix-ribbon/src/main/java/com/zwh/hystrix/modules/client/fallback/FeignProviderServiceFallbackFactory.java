package com.zwh.hystrix.modules.client.fallback;

import com.zwh.hystrix.modules.client.FeignProviderService2Client;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 服务降级，默认返回配置
 * @author zwh
 * @date 2023-3-22
 */
@Component
public class FeignProviderServiceFallbackFactory implements FallbackFactory<FeignProviderService2Client> {
    private static final Logger log = LoggerFactory.getLogger(FeignProviderServiceFallback.class);
    @Override
    public FeignProviderService2Client create(Throwable cause) {
        log.error("xx服务调用失败:{}", cause.getMessage());
        return new FeignProviderService2Client() {

            @Override
            public String providerEcho(String string) {
                return "fallback";
            }

            @Override
            public String providerEcho(String string, Map<String, Object> dto) {
                return "fallback";
            }
        };
    }
}
