package com.zwh.hystrix.modules.client;

import com.zwh.hystrix.modules.client.fallback.FeignProviderServiceFallback;
import com.zwh.hystrix.modules.client.fallback.FeignProviderServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * @author zwh
 * @date 2023-3-22
 */
@FeignClient(value = "service-provider", fallbackFactory = FeignProviderServiceFallbackFactory.class)
public interface FeignProviderService2Client {
    /**
     * provider服务的接口方法，在这里管理
     * @param string
     * @return
     */
    @GetMapping(value = "/echo/{string}")
    String providerEcho(@PathVariable String string);

    @PostMapping(value = "/echo/{string}")
    String providerEcho(@PathVariable String string, @RequestBody Map<String, Object> dto);
}
