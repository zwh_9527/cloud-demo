package com.zwh.hystrix.modules.client.fallback;

import com.zwh.hystrix.modules.client.FeignProviderServiceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 服务降级，默认返回配置
 * @author zwh
 * @date 2023-3-22
 */
@Component
public class FeignProviderServiceFallback implements FeignProviderServiceClient {
    private static final Logger log = LoggerFactory.getLogger(FeignProviderServiceFallback.class);

    @Override
    public String providerEcho(String string) {
        log.error("服务调用失败");
        return "fallback";
    }

    @Override
    public String providerEcho(String string, Map<String, Object> dto) {
        return "fallback";
    }
}
