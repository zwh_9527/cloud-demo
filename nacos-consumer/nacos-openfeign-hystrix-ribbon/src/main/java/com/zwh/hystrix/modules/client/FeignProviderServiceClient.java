package com.zwh.hystrix.modules.client;

import com.zwh.hystrix.modules.client.fallback.FeignProviderServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * fallback 和 fallbackFactory（常用）属性不能同时使用，只能选择其中一种
 * @author zwh
 * @date 2023-3-22
 */
@FeignClient(value = "service-provider", fallback = FeignProviderServiceFallback.class)
public interface FeignProviderServiceClient {
    /**
     * provider服务的接口方法，在这里管理
     * @param string
     * @return
     */
    @GetMapping(value = "/echo/{string}")
    String providerEcho(@PathVariable String string);

    @PostMapping(value = "/echo/{string}")
    String providerEcho(@PathVariable String string, @RequestBody Map<String, Object> dto);
}
