package com.zwh.config;

import feign.Logger;
import feign.Retryer;
import org.springframework.context.annotation.Bean;

/**
 * @author zwh
 * @date 2023-3-23
 */
public class OpenFeignConfig {

    /**
      配置日志级别
     NONE 没有日志
     BASIC 请求方法，请求URL，返回Code和执行时间
     HEADERS 请求和返回的头部基本信息
     FULL 请求和返回的头部，内容，元数据
     * @return
     */
    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }


    /**
     * 默认配置类：{@link org.springframework.cloud.openfeign.FeignClientsConfiguration}
     * @return
     */
    @Bean
    public Retryer feignRetryer() {
        //return Retryer.NEVER_RETRY;
        // 表示每间隔100ms，最大间隔1000ms重试一次，最大重试次数是1，因为第三个参数包含了第一次请求
        return new Retryer.Default(100, 1000, 2);
    }
}
