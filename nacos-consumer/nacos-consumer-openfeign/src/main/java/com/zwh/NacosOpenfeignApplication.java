package com.zwh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * 使用restTemplate进行服务调用
 * Document: https://nacos.io/zh-cn/docs/quick-start-spring-cloud.html
 */
@EnableFeignClients(basePackages = {"com.zwh.modules"})
@SpringBootApplication
@EnableDiscoveryClient
public class NacosOpenfeignApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosOpenfeignApplication.class, args);
    }


    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}


