package com.zwh.modules.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 统一管理服务提供者的api接口，
 * 指定服务提供者的服务名
 * @author zwh
 * @date 2021/7/30 16:58
 **/
@FeignClient(value = "service-provider")
public interface FeignProviderServiceApi {

    /**
     * provider服务的接口方法，在这里管理
     * @param string
     * @return
     */
    @GetMapping(value = "/echo/{string}")
    String providerEcho(@PathVariable String string);

    @PostMapping(value = "/echo/{string}")
    String providerEcho(@PathVariable String string, @RequestBody Map<String, Object> dto);
}
