package com.zwh.modules.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zwh.modules.api.FeignProviderServiceApi;

import java.util.HashMap;

/**
 * @author zwh
 * @date 2021/7/30 17:38
 **/
@Controller
@RequestMapping("testOpenFeign")
public class OpenFeignController {
    @Autowired
    private FeignProviderServiceApi feignProviderServiceApi;

    /**
     * 使用openFeign负载均衡调用远程服务
     * @return
     */
    @GetMapping("executeRemoteMethod")
    public ResponseEntity<String> executeRemoteMethod(){
        return ResponseEntity.ok(feignProviderServiceApi.providerEcho("调用远程服务"));
    }

    /**
     * 使用openFeign负载均衡调用远程服务
     * @return
     */
    @GetMapping("demo2")
    public ResponseEntity<String> demo2(){
        HashMap<String, Object> data = new HashMap<>();
        data.put("name", "zhangwenhe");
        data.put("age", 1);
        return ResponseEntity.ok(feignProviderServiceApi.providerEcho("调用远程服务", data));
    }
}
