package com.zwh.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author zwh
 * @date 2021/7/30 16:23
 **/
@Configuration
public class NacosDiscoveryConfig {
    /**
     * restTemplate 使用负载均衡注解，访问http服务只能用 服务名称访问
     * 不能使用如 http://ip:port/
     * 需要改为 http://serviceId/
     * @return
     */
    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
