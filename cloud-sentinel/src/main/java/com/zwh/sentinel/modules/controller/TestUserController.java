package com.zwh.sentinel.modules.controller;

import com.zwh.sentinel.modules.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zwh
 * @date 2022/3/20
 */
@RestController
public class TestUserController {

    @Autowired
    private UserService userService;

    @GetMapping("/info/{username}")
    public Object info(@PathVariable("username") String username)
    {
        return userService.selectUserByName(username);
    }
}
