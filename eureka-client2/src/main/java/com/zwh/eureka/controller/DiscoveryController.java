package com.zwh.eureka.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author zwh
 * @date 2021/7/9 13:18
 **/
@RestController
public class DiscoveryController {

    @Value("${server.port}")
    private String serverPort;

    @RequestMapping(value = "/echo/{string}", method = RequestMethod.GET)
    public String providerEcho(@PathVariable String string) {
        return "Hello Nacos Discovery,[" + serverPort + "] " + string;
    }

    @PostMapping(value = "/echo/{string}")
    public String providerEcho(@PathVariable String string, @RequestBody Map<String, Object> dto) {
        return "Hello Nacos Discovery,[" + serverPort + "] " + string + JSONObject.toJSONString(dto);
    }
}
