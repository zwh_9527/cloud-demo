package com.zwh.gateway.zuul.config;

import com.zwh.gateway.zuul.custom.CustomHttpClientRibbonCommandFactory;
import com.zwh.gateway.zuul.custom.GatewayServiceTimeoutProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.ribbon.SpringClientFactory;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.cloud.netflix.zuul.filters.route.RibbonCommandFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.Collections;
import java.util.Set;

/**
 * 重写httpClient的配置类，当启用httpclient时
 * @author zwh
 * @date 2023-4-3
 */
@ConditionalOnProperty(name = "ribbon.httpclient.enabled", havingValue = "true", matchIfMissing = false)
@EnableConfigurationProperties({ GatewayServiceTimeoutProperties.class })
@Configuration
public class GatewayServiceTimeoutConfig {

    @Autowired(required = false)
    private Set<FallbackProvider> zuulFallbackProviders = Collections.emptySet();
    @Autowired
    private GatewayServiceTimeoutProperties gatewayServiceTimeoutProperties;

    @Primary
    @Bean
    public RibbonCommandFactory<?> ribbonCommandFactory(
            SpringClientFactory clientFactory, ZuulProperties zuulProperties) {
        return new CustomHttpClientRibbonCommandFactory(clientFactory, zuulProperties,
                zuulFallbackProviders, gatewayServiceTimeoutProperties);
    }

}
