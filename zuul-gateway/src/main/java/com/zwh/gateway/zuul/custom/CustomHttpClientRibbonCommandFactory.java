package com.zwh.gateway.zuul.custom;

import org.springframework.cloud.netflix.ribbon.SpringClientFactory;
import org.springframework.cloud.netflix.ribbon.apache.RibbonLoadBalancingHttpClient;
import org.springframework.cloud.netflix.ribbon.support.RibbonCommandContext;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.cloud.netflix.zuul.filters.route.RibbonCommand;
import org.springframework.cloud.netflix.zuul.filters.route.support.AbstractRibbonCommandFactory;

import java.util.Collections;
import java.util.Set;

/**
 * 自定义httpClient ribbonCommand工厂类，复制HttpClientRibbonCommandFactory类代码
 *
 * @author zwh
 * @date 2023-4-3
 */
public class CustomHttpClientRibbonCommandFactory extends AbstractRibbonCommandFactory {

    private final SpringClientFactory clientFactory;

    private final ZuulProperties zuulProperties;
    private final GatewayServiceTimeoutProperties gatewayServiceTimeoutProperties;

    public CustomHttpClientRibbonCommandFactory(SpringClientFactory clientFactory,
                                                ZuulProperties zuulProperties) {
        this(clientFactory, zuulProperties, Collections.<FallbackProvider>emptySet(), null);
    }

    public CustomHttpClientRibbonCommandFactory(SpringClientFactory clientFactory,
                                                ZuulProperties zuulProperties, Set<FallbackProvider> fallbackProviders) {
        super(fallbackProviders);
        this.clientFactory = clientFactory;
        this.zuulProperties = zuulProperties;
        this.gatewayServiceTimeoutProperties = null;
    }

    public CustomHttpClientRibbonCommandFactory(SpringClientFactory clientFactory,
                                                ZuulProperties zuulProperties,
                                                Set<FallbackProvider> fallbackProviders,
                                                GatewayServiceTimeoutProperties gatewayServiceTimeoutProperties) {
        super(fallbackProviders);
        this.clientFactory = clientFactory;
        this.zuulProperties = zuulProperties;
        this.gatewayServiceTimeoutProperties = gatewayServiceTimeoutProperties;
    }

    @Override
    public RibbonCommand create(RibbonCommandContext context) {
        FallbackProvider zuulFallbackProvider = getFallbackProvider(
                context.getServiceId());
        final String serviceId = context.getServiceId();
        final RibbonLoadBalancingHttpClient client = this.clientFactory
                .getClient(serviceId, RibbonLoadBalancingHttpClient.class);
        client.setLoadBalancer(this.clientFactory.getLoadBalancer(serviceId));
        CustomHttpClientRibbonCommand command = new CustomHttpClientRibbonCommand(serviceId, client, context, zuulProperties,
                zuulFallbackProvider, clientFactory.getClientConfig(serviceId));
        command.setGatewayServiceTimeoutProperties(this.gatewayServiceTimeoutProperties);
        return command;
    }
}
