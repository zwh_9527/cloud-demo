package com.zwh.gateway.zuul.custom;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author zwh
 * @date 2023-4-3
 */
@ConfigurationProperties(prefix = "ribbon.httpclient.custom")
public class GatewayServiceTimeoutProperties {
    private Map<String, Config> serviceIdConfig = new LinkedHashMap<>();

    public Map<String, Config> getServiceIdConfig() {
        return serviceIdConfig;
    }

    public void setServiceIdConfig(Map<String, Config> serviceIdConfig) {
        this.serviceIdConfig = serviceIdConfig;
    }

    public static class Config{
        /**
         * 指的是建立连接所用的时间，适用于网络状况正常的情况下，两端连接所用的时间
         */
        private int connectTimeout;
        /**
         * 指的是建立连接后从服务器读取到可用资源所用时间
         */
        private int readTimeout;

        public int getConnectTimeout() {
            return connectTimeout;
        }

        public void setConnectTimeout(int connectTimeout) {
            this.connectTimeout = connectTimeout;
        }

        public int getReadTimeout() {
            return readTimeout;
        }

        public void setReadTimeout(int readTimeout) {
            this.readTimeout = readTimeout;
        }
    }
}
