package com.zwh.gateway.zuul.custom.route;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Properties;

/**
 * @author zwh
 * @date 2023-4-4
 */
@ConfigurationProperties(prefix = "zuul.custom.dynamic-route.jdbc")
public class RouteJdbcProperties {
    private String url;
    private String username;
    private String password;
    private String driverClassName;

    private Properties druid;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public Properties getDruid() {
        return druid;
    }

    public void setDruid(Properties druid) {
        this.druid = druid;
    }
}
