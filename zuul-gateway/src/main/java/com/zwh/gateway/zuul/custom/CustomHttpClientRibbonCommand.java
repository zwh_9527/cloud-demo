package com.zwh.gateway.zuul.custom;

import com.netflix.client.config.IClientConfig;
import org.springframework.cloud.netflix.ribbon.apache.RibbonApacheHttpRequest;
import org.springframework.cloud.netflix.ribbon.apache.RibbonLoadBalancingHttpClient;
import org.springframework.cloud.netflix.ribbon.support.RibbonCommandContext;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.cloud.netflix.zuul.filters.route.apache.HttpClientRibbonCommand;

/**
 * @author zwh
 * @date 2023-4-3
 */
public class CustomHttpClientRibbonCommand extends HttpClientRibbonCommand {
    private GatewayServiceTimeoutProperties gatewayServiceTimeoutProperties;

    public CustomHttpClientRibbonCommand(String commandKey, RibbonLoadBalancingHttpClient client, RibbonCommandContext context, ZuulProperties zuulProperties) {
        super(commandKey, client, context, zuulProperties);
    }

    public CustomHttpClientRibbonCommand(String commandKey, RibbonLoadBalancingHttpClient client, RibbonCommandContext context, ZuulProperties zuulProperties, FallbackProvider zuulFallbackProvider) {
        super(commandKey, client, context, zuulProperties, zuulFallbackProvider);
    }

    public CustomHttpClientRibbonCommand(String commandKey, RibbonLoadBalancingHttpClient client, RibbonCommandContext context, ZuulProperties zuulProperties, FallbackProvider zuulFallbackProvider, IClientConfig config) {
        super(commandKey, client, context, zuulProperties, zuulFallbackProvider, config);
    }

    @Override
    protected RibbonApacheHttpRequest createRequest() throws Exception {
        CustomRibbonApacheHttpRequest request = new CustomRibbonApacheHttpRequest(this.context);
        request.setGatewayServiceTimeoutProperties(this.gatewayServiceTimeoutProperties);
        return request;
    }

    public void setGatewayServiceTimeoutProperties(GatewayServiceTimeoutProperties gatewayServiceTimeoutProperties) {
        this.gatewayServiceTimeoutProperties = gatewayServiceTimeoutProperties;
    }
}
