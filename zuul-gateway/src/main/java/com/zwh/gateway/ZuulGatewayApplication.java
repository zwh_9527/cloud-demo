package com.zwh.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author zwh
 * @date 2023-3-28
 */
@EnableZuulProxy // zuul网关
@EnableFeignClients // 服务调用
@EnableDiscoveryClient //服务发现
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class ZuulGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZuulGatewayApplication.class, args);
    }
}
