package com.zwh.gateway.controller;

import com.zwh.gateway.zuul.config.RefreshRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zwh
 * @date 2023-4-4
 */
@ConditionalOnProperty(name = "zuul.custom.dynamic-route.enabled", havingValue = "true")
@RestController
public class RefreshController {
    @Autowired
    private RefreshRouteService refreshRouteService;

    @GetMapping("/refresh")
    public String refresh() {
        refreshRouteService.refreshRoute();
        return "refresh";
    }
}
